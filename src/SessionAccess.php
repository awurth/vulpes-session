<?php

namespace Vulpes\Session;

use ArrayAccess;

/**
 * Provide accessing to session data
 * @see \Vulpes\Session\Session
 */
class SessionAccess implements ArrayAccess
{
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $_SESSION);
    }

    public function offsetGet($offset)
    {
        return array_key_exists($offset, $_SESSION) ? $_SESSION[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        $_SESSION[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        if (array_key_exists($offset, $_SESSION)) {
            unset($_SESSION[$offset]);
        }
    }
}