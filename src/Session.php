<?php

namespace Vulpes\Session;

use ArrayAccess;
use Vulpes\Session\Exception\SessionDisabled;

class Session implements ArrayAccess
{
    private array $cookieParams;
    private ArrayAccess $access;

    public function __construct(array $cookieParams, ArrayAccess $access = null)
    {
        $this->cookieParams = $cookieParams;
        $this->access = $access ?: $this->createAccess();
    }

    public function start(): void
    {
        if (session_status() === PHP_SESSION_DISABLED) {
            $this->throwError(new SessionDisabled);
        }
        if (session_status() === PHP_SESSION_ACTIVE) {
            return;
        }
        if ($this->cookieParams) {
            session_set_cookie_params(array_merge(session_get_cookie_params(), $this->cookieParams));
        }
        session_start();
    }

    public function destroy(): void
    {
        $params = session_get_cookie_params();

        $this->setCookie(session_name(), null, [
          'expires'  => time() - 86400,
          'path'     => $params['path'],
          'domain'   => $params['domain'],
          'secure'   => $params['secure'],
          'httponly' => $params['httponly'],
          'samesite' => $params['samesite']
        ]);

        session_unset();
        session_regenerate_id(true);
        session_destroy();
        session_write_close();
    }

    public function getCookie(string $name)
    {
        return isset($_COOKIE) && array_key_exists($name, $_COOKIE) ? $_COOKIE[$name] : null;
    }

    public function setCookie(string $name, $value, array $options = []): void
    {
        if (is_array($value)) {
            $this->setCookieArray($name, $value, $options);
            return;
        }
        if (is_null($value)) {
            $options['expires'] = time() - 86400;
        }
        setcookie($name, $value, $options);
    }

    private function setCookieArray(string $name, array $array, ?array $options): void
    {
        foreach ($array as $key => $value) {
            $this->setCookie($name . '[' . $key . ']', $value, $options);
        }
    }

    public function offsetExists($offset) { return isset($this->access[$offset]); }

    public function offsetGet($offset) { return $this->access[$offset]; }

    public function offsetSet($offset, $value) { $this->access[$offset] = $value; }

    public function offsetUnset($offset) { unset($this->access[$offset]); }

    private function throwError(SessionDisabled $exception) { throw $exception; }

    protected function createAccess(): SessionAccess { return new SessionAccess; }
}