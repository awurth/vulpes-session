**A simple cURL client.**

Example
```php
use Vulpes\Session\Session;
use Vulpes\Session\SessionAccess;

$session = new Session([
    'lifetime' => 86400,
    'domain'   => "example.com",
    'secure'   => true,
    'httponly' => true,
    'samesite' => "Lax"
]);

// start session
$session->start();

// set session value
$session['example'] = 'value';

// get session value
print $session['example'];

// check session value exists
var_dump(isset($session['example']));

// unset session value
unset($session['example']);

// destroy session
$session->destroy();

// get cookie value
$session->getCookie('cookie-name');

// unset cookie
$session->setCookie('cookie-name', null);

// set cookie value (even an array)
$session->setCookie('cookie-name', [
    'array-key' => 'array-value'
], [
    'expires'  => time() + 60 * 60 * 24 * 30, // 30 days
    'path'     => '/',
    'domain'   => 'example.com',
    'secure'   => true,
    'httponly' => true,
    'samesite' => 'Lax'
]);
```